function [energies] = BandEnergy(frame)
    bands = [300 315 335 355 375 400 425 450 475 500 530 560 600 630 670 710 750 800 850 900 950 1000 1060 1120 1180 1250 1320 1400 1500 1600 1700 1800 1900 2000];
    energies = zeros(length(bands)-1,1);
    for i = 1:(length(bands)-1)
        for j = bands(i):bands(i+1)
            energies(i)= energies(i) + frame(j)^2 ;
        end
    end
    energies = energies';
end