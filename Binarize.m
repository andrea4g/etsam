function [F]  = Binarize(E)
    F = zeros(length(E)-1,32);
    for n = 2:(length(E))
        for m= 1:32
            if (E(n,m)-  E(n,m+1) - E(n-1,m) + E(n-1,m+1))>0
               F(n,m) = 1;
            else
                F(n,m)=0;
            end
        end
    end
end