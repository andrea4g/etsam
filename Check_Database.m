function [  ] = Check_Database(  )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    % salvo i dati dei file presenti nelle cartelle
    music = dir('Music\*.mp3');
    fingerprints=dir('Fingerprints\*.mat');
    icon = imread('Images\success.jpg');
    count=0;
    fileUpdated=0;
    fileNotUpdated=0;
    
    % metto a posto i nomi delle fingerprint
    for i=1:length(fingerprints)
        fingerprints(i).name = strrep(fingerprints(i).name, '_and_', ' & ');
        fingerprints(i).name = strrep(fingerprints(i).name, '_', ' ');        
        fingerprints(i).name = strrep(fingerprints(i).name, '.mat', '');
    end
    
    %
    for j=1:length(music)
        info(j)=audioinfo(Path(music,j));
    end
    
    % controllo se le canzoni sono gi� fingerprint
    for j=1:length(music)
        for k=1:length(fingerprints)
            if strcmp(info(j).Title, fingerprints(k).name)==1
                count=count+1;
            end
        end
        
        if count==0
            try
                Fingerprint(info(j).Filename);
                fileUpdated=fileUpdated+1;
            catch 
                fileNotUpdated=fileNotUpdated+1;
            end       
        end
        count=0;
    end
    
    h=msgbox(sprintf('Songs added: \t %d\nSongs not added: \t %d', fileUpdated, fileNotUpdated),'Database updated','custom',icon);
    
end

