function varargout = Etsam(varargin)
% ETSAM MATLAB code for Etsam.fig
%      ETSAM, by itself, creates a new ETSAM or raises the existing
%      singleton*.
%
%      H = ETSAM returns the handle to a new ETSAM or the handle to
%      the existing singleton*.
%
%      ETSAM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ETSAM.M with the given input arguments.
%
%      ETSAM('Property','Value',...) creates a new ETSAM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Etsam_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Etsam_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Etsam

% Last Modified by GUIDE v2.5 23-Jun-2016 11:07:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Etsam_OpeningFcn, ...
                   'gui_OutputFcn',  @Etsam_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Etsam is made visible.
function Etsam_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Etsam (see VARARGIN)

% Choose default command line output for Etsam
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

axes(handles.axes1);
Logo=imread('Images\shazamlogo.jpg');
image(Logo);
axis off;

set(handles.Song_Title, 'string', '-');
% UIWAIT makes Etsam wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Etsam_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Check_Database.
function Check_Database_Callback(hObject, eventdata, handles)
% hObject    handle to Check_Database (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Construct a questdlg with three options
choice = questdlg('Would you like to update the database?', ...
	'Database fingerprint', ...
	'Yes','No','No');
% Handle response
switch choice
    case 'Yes'
        %disp([choice ' coming right up.'])
        Check_Database();
    case 'No'
        %disp([choice ' coming right up.'])
        %close(choice);
end


% --- Executes on button press in Find_Song.
function Find_Song_Callback(hObject, eventdata, handles)
% hObject    handle to Find_Song (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
name=Title();
set(handles.Song_Title, 'string', name);


function Song_Title_Callback(hObject, eventdata, handles)
% hObject    handle to Song_Title (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Song_Title as text
%        str2double(get(hObject,'String')) returns contents of Song_Title as a double


% --- Executes during object creation, after setting all properties.
function Song_Title_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Song_Title (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function Song_Author_Callback(hObject, eventdata, handles)
% hObject    handle to Song_Author (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Song_Author as text
%        str2double(get(hObject,'String')) returns contents of Song_Author as a double


% --- Executes during object creation, after setting all properties.
function Song_Author_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Song_Author (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in song_menu.
function song_menu_Callback(hObject, eventdata, handles)
% hObject    handle to song_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns song_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from song_menu


% --- Executes during object creation, after setting all properties.
function song_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to song_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in internal.
function internal_Callback(hObject, eventdata, handles)
% hObject    handle to internal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run Etsam2
close(Etsam);
