function varargout = Etsam2(varargin)
% ETSAM2 MATLAB code for Etsam2.fig
%      ETSAM2, by itself, creates a new ETSAM2 or raises the existing
%      singleton*.
%
%      H = ETSAM2 returns the handle to a new ETSAM2 or the handle to
%      the existing singleton*.
%
%      ETSAM2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ETSAM2.M with the given input arguments.
%
%      ETSAM2('Property','Value',...) creates a new ETSAM2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Etsam2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Etsam2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Etsam2

% Last Modified by GUIDE v2.5 23-Jun-2016 10:58:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Etsam2_OpeningFcn, ...
                   'gui_OutputFcn',  @Etsam2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Etsam2 is made visible.
function Etsam2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Etsam2 (see VARARGIN)

% Choose default command line output for Etsam2
handles.output = hObject;

axes(handles.axes2);
Logo=imread('Images\shazamlogo.jpg');
image(Logo);
axis off;

set(handles.title2, 'string', '-');
set(handles.my_title, 'string', '');

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Etsam2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Etsam2_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function my_title_Callback(hObject, eventdata, handles)
% hObject    handle to my_title (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of my_title as text
%        str2double(get(hObject,'String')) returns contents of my_title as a double


% --- Executes during object creation, after setting all properties.
function my_title_CreateFcn(hObject, eventdata, handles)
% hObject    handle to my_title (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function title2_Callback(hObject, eventdata, handles)
% hObject    handle to title2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of title2 as text
%        str2double(get(hObject,'String')) returns contents of title2 as a double


% --- Executes during object creation, after setting all properties.
function title2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to title2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in find_title.
function find_title_Callback(hObject, eventdata, handles)
% hObject    handle to find_title (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
icon = imread('Images\loading.png');
h=msgbox('Searching music...','Music search','custom',icon);
name=InternalRecord(get(handles.my_title, 'string'));
delete(h);
set(handles.title2, 'string', name);
