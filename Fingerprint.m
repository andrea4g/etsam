function [ fingerprint ] = Fingerprint( varargin )

    if nargin==1
       FileName = varargin{1};

    h = waitbar(0,'Initializing waitbar...');
    set(h, 'Name','Grinding fingerprint...');
    %Salvo le informazionin del file
    info = audioinfo(FileName);
    %Calcolo FrameLength e FrameStep
    frameinfo = Frame(info.SampleRate);
    %Calcolo il numero di iterazioni da fare prima del file end
    n = floor(info.TotalSamples / frameinfo.FrameStep - 32);
    %Inizializzo matrice E
    E=zeros(n,33);
    %Loop per calcolo fingerprint   
    for i= 1:n
        
        perc = (i*100)/n;
        waitbar(perc/100,h,sprintf('%.2f %% of %s',perc, info.Title))
        
        %Calcolo inizio e fine del frame corrente
        fstart = 1+floor((i-1)*frameinfo.FrameStep);
        fend = fstart + frameinfo.FrameLength;
        samples = [fstart,fend];
        %Leggo il frame corrente dal file
        try
            frame = ToMono(audioread(FileName, samples));
            %Moltiplico per finestra e sovrascrivo
            frame = HannWindow(frame);
            %FFT
            frame = fft(frame);
            %abs
            frame = abs(frame);
            %Divisione in bande e calcolo energia
            E(i,:) = BandEnergy(frame);
        catch
            fprintf('End of file error: frame forced to be equal to previous.\n');
            if i>1
                E(i,:)= E(i-1,:);
            end
        end
    end

    fingerprint = Binarize(E);
    close(h);
    if(strcmp(info.CompressionMethod,'MP3'))
        SaveFP(fingerprint, info);
    end
    
    elseif nargin == 2
        
        FileName = varargin{1};
        samp = varargin{2};
        
    %Salvo le informazionin del file
    info = audioinfo(FileName);
    %Calcolo FrameLength e FrameStep
    frameinfo = Frame(info.SampleRate);
    %Calcolo il numero di iterazioni da fare prima del file end
    n = floor(info.TotalSamples / frameinfo.FrameStep - 32);
    %Inizializzo matrice E
    E=zeros(samp(2)-samp(1),33);
    %Loop per calcolo fingerprint   
    for i= 1:samp(2)-samp(1)        
       
        %Calcolo inizio e fine del frame corrente
        fstart = floor(samp(1)+((i-1)*frameinfo.FrameStep));
        fend = fstart + frameinfo.FrameLength;
        samples = [fstart,fend];
        %Leggo il frame corrente dal file
        frame = ToMono(audioread(FileName, samples));
        %Moltiplico per finestra e sovrascrivo
        frame = HannWindow(frame);
        %FFT
        frame = fft(frame);
        %abs
        frame = abs(frame);
        %Divisione in bande e calcolo energia
        E(i,:) = BandEnergy(frame); 
        
    end

    %eval(sprintf('%s = Binarize(E)', info.Title));
    fingerprint = Binarize(E);
    
    end

end
