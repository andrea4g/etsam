%Script che legge un file audio e dividendo in frame di 370 ms
function [ output ] = Frame(SampleRate)
        %Calcolo lunghezza in campioni (non secondi) del frame
        FrameLength = SampleRate * 0.37;
        %Calcolo lo step di cui fare avanzare il frame ogni passaggio
        FrameStep = FrameLength / 32;
    
        output = struct('FrameLength',FrameLength,'FrameStep',FrameStep);
end
