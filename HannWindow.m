function [ output ] = HannWindow( frame )
%HannWindow Summary of this function goes here
%   Esegue la finestra di hanning sul frame

%finestra della lunghezza del frame
window = hann(length(frame));
%convoluzione
output=frame .* window;

end

