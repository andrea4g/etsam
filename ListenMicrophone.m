function [audio] = ListenMicrophone (duration)
    recObj = audiorecorder(44100,16,2);
    disp('Start recording.')
    recordblocking(recObj, duration);
    disp('End of Recording.');
    audio = getaudiodata(recObj);
    audiowrite('Record\Record.wav',audio,44100);
end