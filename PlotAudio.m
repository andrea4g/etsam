function [] = PlotAudio (FileName)
    [y, Fs] = audioread(FileName);
    fprintf('Plotting...\n');
    plot(y);
end