function [] = SaveFP (f, info)
    t = info.Title;
    t = strrep(t,' ','_');
    t = strrep(t,'&','and');
    eval(sprintf('%s = f',t));
    path = strcat('Fingerprints\',t, '.mat');
    save(path, t);
    fprintf('Fingerprint saved as %s\n',path);
end