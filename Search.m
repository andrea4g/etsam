function [ nomatname ] = Search( fingblock )
        maxpercent = 0; 
        len = length(fingblock);
        bestmatch = struct('name', '', 'fragment', zeros(len,32));
        allfiles = dir('Fingerprints\*.mat');
        for i=1:length(allfiles)
            path = strcat('Fingerprints\',allfiles(i).name);
            load(path);
        
        
        nomatname=strrep(allfiles(i).name,'.mat','');
        var = eval(sprintf(nomatname));
        for j=1:length(var)-(len-1)
            
            submatrix = var(j:(j+(len-1)),:);            
            xorres = xor(submatrix,fingblock);
            
            percent = sum(xorres(:));
            percent = 100 - (percent/(len*32)*100);
            if maxpercent<percent
                maxpercent = percent;
                bestmatch.name = nomatname;
                bestmatch.fragment = submatrix;
            end
            
            
        end
        if maxpercent>80
               maxpercent
               bestmatch.name
               return
        end
            
        end
        maxpercent
        bestmatch.name
        xorres = xor(bestmatch.fragment,fingblock);
        figure(1);
        set(gcf, 'Name', bestmatch.name);
        imshow(bestmatch.fragment);
        
        figure(2);
        set(gcf, 'Name', 'XOR');
        imshow(xorres);
        
        nomatname='Not found';

end

