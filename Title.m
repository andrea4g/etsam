function [ name ] = Title(  )
%Title Summary of this function goes here
%   Detailed explanation goes here
    
    % create Record folder to avoid errors
    if isequal(exist("Record", 'dir'),7) % 7 = directory
    else
      mkdir Record
    end
    
    music = dir('Music\*.mp3');

    %f=Fingerprint(Path(music,10));
    icon = imread('Images\loading.png');
    
    h=msgbox('Recording music...','Music record','custom',icon);    
    audio = ListenMicrophone(5);
    delete(h);
    
    f = Fingerprint('Record\Record.wav');
    
    h=msgbox('Searching song in the database...','Database search','custom',icon);
    fresized = f(101:356, :);
    name=Search(fresized);
    delete(h);

end

