%[s,fS]=audioread('Music\Hymn For the Weekend.mp3');
%tempi1 = (1/fS)*(1:max(size(s)));
s=ToMono('Music\Hymn For the Weekend.mp3');

[Q, tempi]=st_processing(s, 0.37, 31/32*100, fS);

Q1 = Q/max(abs(Q));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%plot part
plot(tempi1,s) ; xlabel('time (s)'); ylabel('s(t)') %Plot Signal
figure;
plot(tempi,Q1); xlabel('time (s)'); ylabel('s(t)') %Plot Windowing Signal
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

L=length(Q);	 	 
NFFT=1024;	 	 
X=fft(Q,NFFT);	 	 
Px=X.*conj(X)/(NFFT*L); %Power of each freq components	 	 
fVals=fS*(0:NFFT/2-1)/NFFT;	 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%plot part
figure;
plot(fVals,Px(1:NFFT/2),'b','LineSmoothing','on','LineWidth',1); %plot FFT	 	 
title('One Sided Power Spectral Density');	 	 
xlabel('Frequency (Hz)')	 	 
ylabel('PSD');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X=abs(X);

